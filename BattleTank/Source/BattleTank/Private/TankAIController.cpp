// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAIController.h"
#include "tankAimingComponent.h"
#include "Tank.h"
#include "GameFramework/Actor.h"

void ATankAIController::SetPawn(APawn * InPawn)
{
	Super::SetPawn(InPawn);
	if (InPawn)
	{
		auto PossessedTank = Cast<ATank>(InPawn);
		if (!ensure(PossessedTank)) { return; }
		// TODO Subscribe our local method to the tank's desth event
		PossessedTank->OnDeath.AddUniqueDynamic(this, &ATankAIController::OnPossessedTankDeath);
	}
}

void ATankAIController::OnPossessedTankDeath()
{
	UE_LOG(LogTemp, Warning, TEXT("Recieved"))
		if (!GetPawn()) { return; }
	GetPawn()->DetachFromControllerPendingDestroy();
}

void ATankAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	auto PlayerPawn = GetWorld()->GetFirstPlayerController()->GetPawn();
	auto ControlledTank = GetPawn();
	auto AimingComponent = ControlledTank->FindComponentByClass<UtankAimingComponent>();
	if (!ensure(PlayerPawn && ControlledTank)) { return; }
	
	// Move towards the player
	MoveToActor(PlayerPawn, AcceptanceRadius);
	// Aim Towards the player
	AimingComponent->AimAt(PlayerPawn->GetActorLocation());

	// fire if ready
	if (AimingComponent->GetFiringState() == EFiringState::Locked)
	{
		AimingComponent->Fire();//TODO dont Fire every frame 
	}
	
	
}


